import os
from astro import bot
try:
    import pyminizip
except Exception:
    os.system("pip install pyminizip")
    import pyminizip

err = "Reply to a File bish"

@bot.on(admin_cmd(pattern="passzip ?(.*)"))
async def pyZip(e):
    if e.fwd_from:
        return
    reply = await e.get_reply_message()
    if not (reply and reply.media):
        return await eod(e, err)
    pass_ = e.pattern_match.group(1)
    eris = await eor(e, "-->downloading..<--")
    dl_ = await e.client.download_media(reply)
    await eris.edit("-->compressing..<--")
    nem_ = reply.file.name
    zip_ = f"{nem_}.zip" if nem_ else "Astro_Zip.zip"
    password = pass_ if pass_ else "astro"
    cap_ = f"**File Name :** - {zip_} \n"\
    f"**Password to Unzip :** - `{password}`"
    
    pyminizip.compress(
        dl_, None, zip_, password, 5)
    await eris.edit("-->uploading..<--")
    try:
        await e.client.send_file(
            e.chat_id, zip_, caption=cap_)
        await eris.delete()
    except Exception as ex:
        return await eris.edit(f"#Error: {ex}")
    finally:
        os.remove(zip_)
        os.remove(dl_)