import asyncio
from astro import CMD_HELP
from astro.utils import admin_cmd


@bot.on(admin_cmd(pattern="lovu ?(.*)"))
async def _(event):
    if not event.text[0].isalpha() and event.text[0] not in ("/", "#", "@", "!"):
        await event.edit("love u 😘")
        await asyncio.sleep(0.2)
        await event.edit("love u bro")
        await asyncio.sleep(0.2)
        await event.edit("love u babes")
        await asyncio.sleep(0.2)
        await event.edit("love u sir")
        await asyncio.sleep(0.2)
        await event.edit("love u sister")
        await asyncio.sleep(0.2)
        await event.edit("love u lol")
        await asyncio.sleep(0.2)
        await event.edit(
            "╔╗─╔═╗╔╗─╔╗╔═╗     ╔╦╗\n║║─║║║║╚╦╝║║╦╝     ║║║\n║╚╗║║║╚╗║╔╝║╩╗     ║║║\n╚═╝╚═╝─╚═╝─╚═╝     ╚═╝"
        )


@bot.on(admin_cmd(pattern="plz ?(.*)"))
async def _(event):
    if not event.text[0].isalpha() and event.text[0] not in ("/", "#", "@", "!"):
        await event.edit("please🥺🥺🥺🥺🥺 ")
        await asyncio.sleep(1.3)
        await event.edit(
            "╔═╗╔╗─╔═╗╔══╗╔══╗╔═╗\n║╬║║║─║╦╝║╔╗║║══╣║╦╝\n║╔╝║╚╗║╩╗║╠╣║╠══║║╩╗\n╚╝─╚═╝╚═╝╚╝╚╝╚══╝╚═╝"
        )


@bot.on(admin_cmd(pattern="wtbf ?(.*)"))
async def _(event):
    if not event.text[0].isalpha() and event.text[0] not in ("/", "#", "@", "!"):
        await event.edit("what the fuck bro")
        await asyncio.sleep(0.2)
        await event.edit("what the fuck lol")
        await asyncio.sleep(0.2)
        await event.edit("what the fuck idiot")
        await asyncio.sleep(0.2)
        await event.edit("╔╦═╦╗╔══╗╔══╗\n║║║║║╚╗╔╝║═╦╝\n║║║║║─║║─║╔╝─\n╚═╩═╝─╚╝─╚╝──")


@bot.on(admin_cmd(pattern="fyes ?(.*)"))
async def _(event):
    if not event.text[0].isalpha() and event.text[0] not in ("/", "#", "@", "!"):
        await event.edit("yes,ehy not")
        await asyncio.sleep(1)
        await event.edit("╔═╦╗╔═╗╔══╗\n╚╗║║║╦╝║══╣\n╔╩╗║║╩╗╠══║\n╚══╝╚═╝╚══╝")


@bot.on(admin_cmd(pattern="fno ?(.*)"))
async def _(event):
    if not event.text[0].isalpha() and event.text[0] not in ("/", "#", "@", "!"):
        await event.edit("no never ")
        await asyncio.sleep(1)
        await event.edit("╔═╦╗╔═╗\n║║║║║║║\n║║║║║║║\n╚╩═╝╚═╝")


@bot.on(admin_cmd(pattern="fbad ?(.*)"))
async def _(event):
    if not event.text[0].isalpha() and event.text[0] not in ("/", "#", "@", "!"):
        await event.edit("very bad , i didnt like it")
        await asyncio.sleep(1)
        await event.edit("╔══╗╔══╗╔══╗\n║╔╗║║╔╗║╚╗╗║\n║╔╗║║╠╣║╔╩╝║\n╚══╝╚╝╚╝╚══╝")


@bot.on(admin_cmd(pattern="fgd ?(.*)"))
async def _(event):
    if not event.text[0].isalpha() and event.text[0] not in ("/", "#", "@", "!"):
        await event.edit(
            "╔═╗───────╔╗ ─╔╗────╔╦╗\n║╬║╔═╗╔═╗╔╝║ ╔╝║╔═╗─║║║\n╠╗║║╬║║╬║║╬║ ║╬║║╬╚╗╠╗║\n╚═╝╚═╝╚═╝╚═╝ ╚═╝╚══╝╚═╝"
        )


@bot.on(admin_cmd(pattern="noice ?(.*)"))
async def _(event):
    if not event.text[0].isalpha() and event.text[0] not in ("/", "#", "@", "!"):
        await event.edit(
            "╔═╦╗╔══╗╔═╗╔═╗\n║║║║╚║║╝║╔╝║╦╝\n║║║║╔║║╗║╚╗║╩╗\n╚╩═╝╚══╝╚═╝╚═╝"
        )


@bot.on(admin_cmd(pattern="really ?(.*)"))
async def _(event):
    if not event.text[0].isalpha() and event.text[0] not in ("/", "#", "@", "!"):
        await event.edit(
            "╔═╗╔═╗╔══╗╔╗─╔╗─╔═╦╗\n║╬║║╦╝║╔╗║║║─║║─╚╗║║\n║╗╣║╩╗║╠╣║║╚╗║╚╗╔╩╗║\n╚╩╝╚═╝╚╝╚╝╚═╝╚═╝╚══╝"
        )


@bot.on(admin_cmd(pattern="ooh ?(.*)"))
async def _(event):
    if not event.text[0].isalpha() and event.text[0] not in ("/", "#", "@", "!"):
        await event.edit("╔═╗╔═╗╔═╗╔╗╔╗\n║║║║║║║║║║╚╝║\n║║║║║║║║║║╔╗║\n╚═╝╚═╝╚═╝╚╝╚╝")


@bot.on(admin_cmd(pattern="maker ?(.*)"))
async def _(event):
    if not event.text[0].isalpha() and event.text[0] not in ("/", "#", "@", "!"):
        await event.edit(
            "╔══╗╔══╗╔╦═╦╗╔══╗╔═╦╗\n║══╣║╔╗║║║║║║║╔╗║║║║║\n╠══║║╠╣║║║║║║║╠╣║║║║║\n╚══╝╚╝╚╝╚═╩═╝╚╝╚╝╚╩═╝"
        )


@bot.on(admin_cmd(pattern="pgl ?(.*)"))
async def _(event):
    if not event.text[0].isalpha() and event.text[0] not in ("/", "#", "@", "!"):
        await event.edit(
            "╔═╗╔══╗╔══╗╔══╗╔╗─\n║╬║║╔╗║║╔═╣║╔╗║║║─\n║╔╝║╠╣║║╚╗║║╠╣║║╚╗\n╚╝─╚╝╚╝╚══╝╚╝╚╝╚═╝"
        )
        
        
@bot.on(admin_cmd(pattern="mst ?(.*)"))
async def _(event):
    if not event.text[0].isalpha() and event.text[0] not in ("/", "#", "@", "!"):
        await event.edit("mst hu bbro ")
        await asyncio.sleep(1)
        await event.edit("╔═╦═╗╔══╗╔══╗\n║║║║║║══╣╚╗╔╝\n║║║║║╠══║─║║─\n╚╩═╩╝╚══╝─╚╝─")


@bot.on(admin_cmd(pattern="gmg ?(.*)"))
async def _(event):
    if not event.text[0].isalpha() and event.text[0] not in ("/", "#", "@", "!"):
        await event.edit("good morning ")
        await asyncio.sleep(1)
        await event.edit("╔══╗╔═╦═╗\n║╔═╣║║║║║\n║╚╗║║║║║║\n╚══╝╚╩═╩╝")


@bot.on(admin_cmd(pattern="good ?(.*)"))
async def _(event):
    if not event.text[0].isalpha() and event.text[0] not in ("/", "#", "@", "!"):
        await event.edit(
            "╔══╗╔═╗╔═╗╔══╗\n║╔═╣║║║║║║╚╗╗║\n║╚╗║║║║║║║╔╩╝║\n╚══╝╚═╝╚═╝╚══╝"
        )


@bot.on(admin_cmd(pattern="srry ?(.*)"))
async def _(event):
    if not event.text[0].isalpha() and event.text[0] not in ("/", "#", "@", "!"):
        await event.edit("i'm sorry")
        await asyncio.sleep(1)
        await event.edit("last time forgive me")
        await asyncio.sleep(1)
        await event.edit(
            "╔══╗╔═╗╔═╗╔═╗╔═╦╗\n║══╣║║║║╬║║╬║╚╗║║\n╠══║║║║║╗╣║╗╣╔╩╗║\n╚══╝╚═╝╚╩╝╚╩╝╚══╝"
        )


@bot.on(admin_cmd(pattern="thnx ?(.*)"))
async def _(event):
    if not event.text[0].isalpha() and event.text[0] not in ("/", "#", "@", "!"):
        await event.edit("thanks for your help")
        await asyncio.sleep(1)
        await event.edit(
            "╔══╗╔╗╔╗╔══╗╔═╦╗╔╦╗╔══╗\n╚╗╔╝║╚╝║║╔╗║║║║║║╔╝║══╣\n─║║─║╔╗║║╠╣║║║║║║╚╗╠══║\n─╚╝─╚╝╚╝╚╝╚╝╚╩═╝╚╩╝╚══╝"
        )


@bot.on(admin_cmd(pattern="ok ?(.*)"))
async def _(event):
    if not event.text[0].isalpha() and event.text[0] not in ("/", "#", "@", "!"):
        await event.edit("▒▐█▀▀█▌▒▐█▒▐▀\n▒▐█▄▒█▌▒▐██▌░\n▒▐██▄█▌▒▐█▒▐▄")


@bot.on(admin_cmd(pattern="smile ?(.*)"))
async def _(event):
    if not event.text[0].isalpha() and event.text[0] not in ("/", "#", "@", "!"):
        await event.edit("how sad ")
        await asyncio.sleep(1)
        await event.edit(
            "╔══╗╔═╦═╗╔══╗╔╗─╔═╗\n║══╣║║║║║╚║║╝║║─║╦╝\n╠══║║║║║║╔║║╗║╚╗║╩╗\n╚══╝╚╩═╩╝╚══╝╚═╝╚═╝"
        )


@bot.on(admin_cmd(pattern="lal ?(.*)"))
async def _(event):
    if not event.text[0].isalpha() and event.text[0] not in ("/", "#", "@", "!"):
        await event.edit("╔╗─╔═╗╔╗─\n║╚╗║╬║║╚╗\n╚═╝╚═╝╚═╝")
        
        
CMD_HELP.update({"BigArt": "lal - use: LoL type art\
\nsmile - use: Smile Typed art\
\nok - use: ok Art\
\nthnx - big thanks\
\nsrry - Say Sorry\
\ngood - Have a good message\
\nmst - Big Mast\
\ngmg - good morning\
\npgl - oye pagal🥴\
\nmaker - Try your self🥴\
\nohh - big ohh\
\nreally - big really message\
\nnoice - Big Nice\
\nfbad - try your self\
\nfgd - try your self\
\nfno - say no\
\nfyes - say yes\
\nwtbf - what the fuck\
\nplz - Have some plz\
\nlovu - love u baby"
  
})